import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { TemplateService } from '../services/template.service';
import * as template from '../actions/template.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

@Injectable()
export class TemplateEffects {
  constructor(
    private templateService: TemplateService,
    private actions$: Actions
  ) { }

  @Effect() get$ = this.actions$
      .ofType(template.LOAD)
      .switchMap(payload => this.templateService.get()
        // If successful, dispatch success action with result
        .map(res => {
            console.log(res);
            return { type: template.LOAD_SUCCESS, payload: res };
        })
        // If request fails, dispatch failed action
        .catch(() => Observable.of({ type: template.LOAD_FAIL}))
      );
}