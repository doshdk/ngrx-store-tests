import { Action } from '@ngrx/store';

export const LOAD =                 '[Template] Load';
export const LOAD_SUCCESS =         '[Template] Load Success';
export const LOAD_FAIL =            '[Template] Load Fail';

/**
 * Load Template Actions
 */
export class LoadAction implements Action {
  readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: any) { }
}

export class LoadFailAction implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export type Actions =
  | LoadAction
  | LoadSuccessAction
  | LoadFailAction;