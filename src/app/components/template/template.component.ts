import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromTemplate from '../../reducers';
import * as template from '../../actions/template.actions';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  loading$: Observable<boolean>

  constructor(private store: Store<fromTemplate.State>) {
    this.loading$ = this.store.select(fromTemplate.getTemplateLoading);
  }

  ngOnInit() {
    this.store.dispatch(new template.LoadAction);
  }

}
