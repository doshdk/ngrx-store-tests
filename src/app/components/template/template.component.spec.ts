import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { MockStore } from './../../mocks/store.mock';
import { Store } from '@ngrx/store';

import { TemplateComponent } from './template.component';

describe('TemplateComponent', () => {
  let component: TemplateComponent;
  let fixture: ComponentFixture<TemplateComponent>;
  let de: DebugElement;
  let _titleEl: HTMLElement;
  let _store: any;

  const templates = [
    {title: 'hello'},
    {title: 'hello2'},
    {title: 'hello3'},
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateComponent ],
      providers: [
        {provide: Store, useValue: new MockStore({template: {
          loading: false
        }})}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateComponent);
    _store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('Display Tags title', () => {
    _store.next({template: {
      loading: true
    }});
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('h1'));
    _titleEl = de.nativeElement;
    expect(_titleEl.textContent).toContain('template works!');
  });


});

