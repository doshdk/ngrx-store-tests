import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class TemplateService {
  
  constructor() { }

  get(): Observable<any[]> {
    return Observable.of([
      {title: 'hello'},
      {title: 'hello2'},
      {title: 'hello3'},
    ]);
  }


}