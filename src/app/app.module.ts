import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { reducers } from './reducers';
import { TemplateEffects } from './effects/template.effects';
import { TemplateComponent } from './components/template/template.component';
import { TemplateService } from './services/template.service';

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25 //  Retains last 25 states
    }),
    EffectsModule.forRoot([TemplateEffects])
  ],
  providers: [TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
