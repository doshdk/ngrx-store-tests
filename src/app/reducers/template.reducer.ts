import * as template from '../actions/template.actions';

export interface State {
  loading: boolean;
  entities: { [id: string]: any };
  result: string[];
  templates: any[] | null;
}

export const initialState: State = {
  loading: false,
  entities: {},
  result: [],
  templates: null
}

export function reducer(state = initialState, action: template.Actions): State {
  switch (action.type) {
    case template.LOAD: {
      return {
        ...state,
        loading: true
      }
    }

    case template.LOAD_SUCCESS: {

      return {
        ...state,
        loading: false,
        templates: action.payload
      };
    }

     case template.LOAD_FAIL: {

      return {
        ...state,
        loading: false,
      };
    }

    default: {
      return state;
    }
  }
}

export const getLoadingStatus = (state: State) => state.loading;