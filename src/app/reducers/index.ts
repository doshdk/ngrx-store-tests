import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    MetaReducer,
} from '@ngrx/store';

import * as fromTemplate from './template.reducer';

export interface State {
    template: fromTemplate.State;
}

export const reducers: ActionReducerMap<State> = {
    template: fromTemplate.reducer
};

export const getTemplateState = (state: State) => state.template;
export const getTemplateLoading = createSelector(getTemplateState, fromTemplate.getLoadingStatus);