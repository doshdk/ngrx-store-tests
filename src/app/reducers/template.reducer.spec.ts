import { reducer } from '../reducers/template.reducer';
import * as fromTemplate from '../reducers/template.reducer';

describe('TemplateReducer', () => {

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(fromTemplate.initialState);
    });
  });

});